var rule = {
    title: '非凡资源',
    host: 'http://api.ffzyapi.com',
    homeTid: '13',
    homeUrl: '/api.php/provide/vod/?ac=detail&t={{rule.homeTid}}',
    //url: '/api.php/provide/vod?ac=detail&t=fyclass&pg=fypage&f=',//网站的分类页面链接
    url: '/index.php/ajax/data?mid=1&tid=fyfilter&page=fypage&limit=30',//网站的分类页面链接
    class_name: '电影&电视剧&综艺&动漫',//静态分类名称拼接
    class_url: '1&2&3&4',//静态分类标识拼接
    searchUrl: '/api.php/provide/vod?ac=detail&wd=**&pg=fypage',
    detailUrl: '/api.php/provide/vod?ac=detail&ids=fyid',
    searchable: 2,//是否启用全局搜索,
    quickSearch: 0,//是否启用快速搜索,
    filterable: 1,//是否启用筛选,
    filter_url: '{{fl.cateId}}',
    filter: {
        "1": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "1" }, { "n": "动作片", "v": "6" }, { "n": "喜剧片", "v": "7" }, { "n": "爱情片", "v": "8" }, { "n": "科幻片", "v": "9" }, { "n": "恐怖片", "v": "10" }, { "n": "剧情片", "v": "11" }, { "n": "战争片", "v": "12" }, { "n": "记录片", "v": "20" }, { "n": "预告片", "v": "45" }] }],
        "2": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "2" }, { "n": "国产剧", "v": "13" }, { "n": "香港剧", "v": "14" }, { "n": "韩国剧", "v": "15" }, { "n": "欧美剧", "v": "16" }, { "n": "台湾剧", "v": "21" }, { "n": "日本剧", "v": "22" }, { "n": "海外剧", "v": "23" }, { "n": "泰国剧", "v": "24" }] }],
        "3": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "3" }, { "n": "大陆综艺", "v": "25" }, { "n": "港台综艺", "v": "26" }, { "n": "日韩综艺", "v": "27" }, { "n": "欧美综艺", "v": "28" }] }],
        "4": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "4" }, { "n": "国产动漫", "v": "29" }, { "n": "日韩动漫", "v": "30" }, { "n": "欧美动漫", "v": "31" }, { "n": "港台动漫", "v": "32" }, { "n": "海外动漫", "v": "33" }] }]
    },
    filter_def: {
        1: { cateId: '1' },
        2: { cateId: '2' },
        3: { cateId: '3' },
        4: { cateId: '4' }
    },
    play_parse: true,
    //js免嗅
    lazy: `js:
          input="https://sn.953365.cn/lzff.php?url=" + input.split("?")[0];
          log(input);
          let html=JSON.parse(request(input));
           input = {
           parse: 0,
           url: html.url}
           `,
    multi: 1,
    timeout: 5000,//网站的全局请求超时,默认是3000毫秒
    limit: 20,// 首页推荐显示数量
    tab_remove: ['feifan'], //移除某个线路及相关的选集
    推荐: 'json:list;vod_name;vod_pic;vod_remarks;vod_id', // double: true, // 推荐内容是否双层定位
    //一级: 'json:list;vod_name;vod_pic;vod_remarks;vod_id',
    一级: `js:
        let d = [];
        // 忽略分类
        let cate_exclude = '34';
        let html = request(input);
        let list = JSON.parse(html).list;
        list.forEach(function (it){
            if(!cate_exclude.match(it.type_id)){
                d.push({
                    title:it.vod_name,
                    img:it.vod_pic,
                    desc:it.type_name,
                    url:it.vod_id
                });
            }
        });
        setResult(d);
    `,
    /**
     * 资源采集站，二级链接解析
     */
    //二级: `json:list;vod_name;vod_pic;vod_remarks;vod_id`,
    二级: `js:
        let html = request(input);
        let list = JSON.parse(html).list;
        if(list.length===1){
           VOD = list[0];
            VOD.vod_blurb = VOD.vod_blurb.replace(/　/g, '').replace(/<[^>]*>/g, '');
            VOD.vod_content = VOD.vod_content.replace(/　/g, '').replace(/<[^>]*>/g, '');
        }
    `,
    /**
     * 搜索解析 过滤部分资源
     */
    //搜索: 'json:list;vod_name;vod_pic;vod_remarks;vod_id',
    搜索: `js:
        let d = [];
        // 忽略分类
        let cate_exclude = '34';
        let html = request(input);
        let list = JSON.parse(html).list;
        list.forEach(function (it){
            if(!cate_exclude.match(it.type_id)){
                d.push({
                    title:it.vod_name,
                    img:it.vod_pic,
                    desc:it.type_name,
                    url:it.vod_id
                });
            }
        });
        setResult(d);
    `,
}

