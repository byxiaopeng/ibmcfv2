var rule = {
    title: '量子资源',
    host: 'https://cj.lziapi.com',
    url: '/api.php/provide/vod?ac=detail&t=fyclass&pg=fypage',
    homeUrl: '/api.php/provide/vod?ac=detail&t=13',
    //searchUrl: '/api.php/provide/vod?ac=detail&wd=**&pg=fypage',
    searchUrl: 'https://search.lziapi.com/json-api/?dname=liangzi&key=**&count=20&pg=fypage',
    detailUrl: '/api.php/provide/vod?ac=detail&ids=fyid',
    searchable: 2,
    quickSearch: 0,
    filterable: 0,
    class_parse: 'json:class;',
    cate_exclude: '电影片|连续剧|综艺片|动漫片|演员|体育|伦理片',
    headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36'
    },
    play_parse: true,   
    lazy: `js:
          input="https://sn.953365.cn/lzff.php?url=" + input;
          log(input)
          let html=JSON.parse(request(input));
           input = {
           parse: 0,
           url: html.url}
        `,
    /*
    proxy_rule: `js:
    let url = input.url;
    let m3u8 = fixAdM3u8Ai(url);
    input = [200,'application/vnd.apple.mpegurl',m3u8]
    `,
    */
    multi: 1,
    timeout: 5000,//网站的全局请求超时,默认是3000毫秒
    limit: 20,// 首页推荐显示数量
    tab_remove: ['liangzi'], //移除某个线路及相关的选集
    推荐: 'json:list;vod_name;vod_pic;vod_remarks;vod_id;vod_content', //
    //列表;标题;图片;副标题;链接;详情 其中最后一个参数选填
    一级: 'json:list;vod_name;vod_pic;vod_remarks;vod_id;vod_content',
    /**
     * 资源采集站，二级链接解析
     */
    //二级: `json:list;vod_name;vod_pic;vod_remarks;vod_id`,

    // 二级可以是*,表示规则无二级,直接拿一级的链接进行嗅探
    // 二级 title: 片名;类型
    // 二级 desc: 主要信息;年代;地区;演员;导演
    // content   内容简介
    // 或者 {title:'',img:'',desc:'',content:'',tabs:'',lists:''}
    二级: `js:
        let html = request(input);
        let list = JSON.parse(html).list;
        if(list.length===1){
           VOD = list[0];
            VOD.vod_blurb = VOD.vod_blurb.replace(/　/g, '').replace(/<[^>]*>/g, '');
            VOD.vod_content = VOD.vod_content.replace(/　/g, '').replace(/<[^>]*>/g, '');
        }
    `,
    /**
     * 搜索解析 过滤部分资源
     */
    //搜索: '*',
    搜索: `js:
        let d = [];
        // 忽略分类
        let cate_exclude = '34';
        let html = request(input);
        let list = JSON.parse(html).posts;
        list.forEach(function (it){
            if(!cate_exclude.match(it.type_id)){
                d.push({
                    title:it.vod_name,
                    img:it.vod_pic,
                    desc:it.type_name,
                    url:it.vod_id
                });
            }
        });
        setResult(d);
    `,
}
