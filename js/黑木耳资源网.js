var rule = {
    title: '黑木耳资源',
    host: 'https://json02.heimuer.xyz',
    //url: '/api.php/provide/vod?ac=detail&t=fyclass&pg=fypage&year=2024',
    url: '/index.php/ajax/data?mid=1&tid=fyfilter&page=fypage&limit=20',
    class_name: '电影&电视剧&动漫&综艺&短剧',
    class_url: '1&2&3&4&27',
    homeUrl: '/api.php/provide/vod?ac=detail',
    searchUrl: '/api.php/provide/vod?ac=detail&wd=**&pg=fypage',
    detailUrl: '/api.php/provide/vod?ac=detail&ids=fyid',
    searchable: 2,
    quickSearch: 0,
    filterable: 1,
    filter_url: '{{fl.cateId}}',
    filter: {
        "1": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "" }, { "n": "剧情片", "v": "6" }, { "n": "动作片", "v": "7" }, { "n": "冒险", "v": "8" }, { "n": "同性", "v": "9" }, { "n": "喜剧", "v": "10" }, { "n": "奇幻", "v": "11" }, { "n": "恐怖", "v": "12" }, { "n": "悬疑", "v": "20" }, { "n": "惊悚", "v": "21" }, { "n": "灾难", "v": "22" }, { "n": "爱情", "v": "23" }, { "n": "犯罪", "v": "24" }, { "n": "科幻", "v": "25" }, { "n": "动画电影", "v": "26" }, { "n": "歌舞", "v": "33" }, { "n": "战争", "v": "34" }, { "n": "经典", "v": "35" }, { "n": "网络电影", "v": "36" }, { "n": "其它", "v": "37" }] }],
        "2": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "" }, { "n": "国产剧", "v": "13" }, { "n": "港剧", "v": "14" }, { "n": "韩剧", "v": "15" }, { "n": "日剧", "v": "16" }, { "n": "泰剧", "v": "28" }, { "n": "台剧", "v": "29" }, { "n": "欧美剧", "v": "30" }, { "n": "新马剧", "v": "31" }, { "n": "其他剧", "v": "32" }] }],
        "4": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "" }, { "n": "国产综艺", "v": "38" }, { "n": "港台综艺", "v": "39" }, { "n": "韩国综艺", "v": "40" }, { "n": "日本综艺", "v": "41" }, { "n": "欧美综艺", "v": "42" }, { "n": "新马泰综艺", "v": "43" }, { "n": "其他综艺", "v": "44" }] }],
        "3": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "" }, { "n": "欧美", "v": "57" }, { "n": "日本", "v": "58" }, { "n": "韩国", "v": "59" }, { "n": "国产", "v": "60" }, { "n": "港台", "v": "61" }, { "n": "新马泰", "v": "62" }, { "n": "其它", "v": "63" }] }],
        "27": [{ "key": "cateId", "name": "剧情", "value": [{ "n": "全部", "v": "" }, { "n": "虐恋", "v": "45" }, { "n": "虐恋", "v": "46" }, { "n": "逆袭", "v": "47" }, { "n": "悬疑", "v": "48" }, { "n": "神豪", "v": "49" }, { "n": "重生", "v": "50" }, { "n": "复仇", "v": "51" }, { "n": "穿越", "v": "52" }, { "n": "甜宠", "v": "53" }, { "n": "强者", "v": "54" }, { "n": "萌宝", "v": "55" }, { "n": "其它", "v": "56" }] }]
    },
    filter_def: {
        1: { cateId: '1' },
        2: { cateId: '2' },
        4: { cateId: '4' },
        3: { cateId: '3' },
        27: { cateId: '27' }
    },
    play_parse: true,
    lazy:`js:
          input="https://jf.vidz.asia/api.php?url=" + input;
          let html=JSON.parse(request(input));
           input = {
           parse: 0,
           url: html.url}
           `,
    multi: 1,
    limit: 20,
    推荐: 'json:list;vod_name;vod_pic;vod_remarks;vod_id',
    一级: 'json:list;vod_name;vod_pic;vod_remarks;vod_id',
    //二级: `json:list;vod_name;vod_pic;vod_remarks;vod_id`,
    二级: `js:
        let html = request(input);
        let list = JSON.parse(html).list;
        if(list.length===1){
           VOD = list[0];
            VOD.vod_blurb = VOD.vod_blurb.replace(/　/g, '').replace(/<[^>]*>/g, '');
            VOD.vod_content = VOD.vod_content.replace(/　/g, '').replace(/<[^>]*>/g, '');
        }
    `,
    搜索: 'json:list;vod_name;vod_pic;vod_remarks;vod_id',
}
